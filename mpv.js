var PLUGIN_INFO = xml`
<VimperatorPlugin>
    <name>mpv.js</name>
    <description>Play pages in MPV</description>
    <version>0.1</version>
</VimperatorPlugin>`;


(function () {

    options.add(["mpvquality", "mpv"],
            "define youtube-dl/MPV quality",
            "string", "bestvideo[height < 1440]+bestaudio/best");

    mappings.addUserMap([modes.NORMAL],
            ["mp"],
            "Open page in MPV",
            function () { open(buffer.URL) });

    hints.addMode("m",
            "Open with MPV",
            function (elem) { open(elem.href) });

    function open (url) {
        let file = FileUtils.File("/usr/bin/mpv");
        let process = Cc["@mozilla.org/process/util;1"].createInstance(Ci.nsIProcess);

        let args = ["--ytdl", "--ytdl-format=" + options["mpvquality"], url];

        process.init(file);
        if ('runw' in process) {
            process.runw(false, args, args.length);
        } else {
            process.run(false, args, args.length);
        }
    }

})();
//vim:sw=4 ts=4 et:
