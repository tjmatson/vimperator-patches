MPV plugin - Vimperator plugin that allows you to open links and pages in MPV through youtube-dl.
==========

Usage
-----

* __;m__ to open an element in MPV using ExtendedHint mode.

* __mp__ to open the current page in MPV.

Installation
------------

```
$ git clone git@gitlab.com:tjmatson/vimperator-patches.git
$ cp vimperator-mpv/mpv.js ~/.vimperator/plugin
```

zIndex patch - Forces hints to be on top of all other elements.
============

Installation
------------

Vimperator source code can be gotten at:

https://github.com/vimperator/vimperator-labs

```
$ git clone git@gitlab.com:tjmatson/vimperator-patches.git
$ git clone git@github.com:vimperator/vimperator-labs.git
$ cd vimperator-labs/
$ patch -p1 < ../vimperator-patches/zindex.patch
$ make && make install
```

In order to install the patched extension, open the .xpi file in Firefox.
